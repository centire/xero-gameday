﻿exports.handler = function( event, context )
{
    // modules for xero
    var fs = require( 'fs' );
    var crypto = require( "crypto" );
    var oauth = require( "oauth" );
    var EasyXml = require( 'easyxml' );
    var xml2js = require( 'xml2js' );
    var inflect = require( 'inflect' );
    var XERO_BASE_URL = 'https://api.xero.com';
    var XERO_API_URL = XERO_BASE_URL + '/api.xro/2.0';
    var rsa_key = fs.readFileSync( 'privatekey.pem' );
    var key = 'BGL5WZNJWIMMGUIMUGRRDHPZT4CUPC';
    var secret = 'PHGCCZSVNJJBNN8XIFJIOG6B4VJWUT';

    // Gameday API token
    var API_TOKEN = '3xZG1rMV288CQ01kywDz6RTBMdp9YfkJjbTBnurz';

    // Host name
    var HOST = 'gamedaymouthguards.com.au';

    // Api path
    var API_PATH = '/api/v2/';

    // Start date
    //var START_DATE = "2015-12-25T14:05:20-0800"; // Leave blank if you want all products and use same format
    var START_DATE = ""; // Leave blank if you want all products and use same format

    // End Date
    //var END_DATE = "2016-02-27T14:05:20-0800"; // Leave blank if you want all products and use same format
    var END_DATE = ""; // Leave blank if you want all products and use same format

    var ORDERS_ORDER_BY_COLUMN = "id"; // Do not leave blank this one.The orders comes order by id if you want to change just put the column name

    var ORDER_BY = "DESC";     // Do not leave blank this one you can change for ascending ASC

    // Assign empty
    var body = '';

    if( START_DATE == "" || END_DATE == "" )
    {
        // Get today date
        var today = new Date();

        var dd = today.getDate();
        dd = zeroAppendBeforeDigit( dd );
        var month = today.getMonth() + 1; //January is 0!
        month = zeroAppendBeforeDigit( month );
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        hh = zeroAppendBeforeDigit( hh );
        var mm = today.getMinutes();
        mm = zeroAppendBeforeDigit( mm );
        var ss = today.getSeconds();
        ss = zeroAppendBeforeDigit( ss );

        END_DATE = yyyy + '-' + month + '-' + dd + 'T' + hh + ':' + mm + ':' + ss + '-0800';

        // For yesterday
        var yesterday = new Date( today );
        yesterday.setDate( today.getDate() - 1 );

        dd = yesterday.getDate();
        dd = zeroAppendBeforeDigit( dd );
        month = yesterday.getMonth() + 1; //January is 0!
        month = zeroAppendBeforeDigit( month );
        yyyy = yesterday.getFullYear();

        START_DATE = yyyy + '-' + month + '-' + dd + 'T' + hh + ':' + mm + ':' + ss + '-0800';

    }

    var query_parms = 'created_at_min=' + START_DATE + '&created_at_max=' + END_DATE + '&';

    // Set requested URLS
    var requests = [
        'orders?sort=' + ORDERS_ORDER_BY_COLUMN + '&order=' + ORDER_BY + '&' + query_parms + 'embed=customer.address,items,invoices.payments,tracking_codes,card_token'
    ];

    // HTTPS protocol use
    var http = require( 'https' );
    var post_options;
    var post_req;

    for( request = 0; request < requests.length; request ++ )
    {
        // Request options
        post_options = {
            host: HOST,
            path: API_PATH + requests[request],
            method: 'GET',
            headers: {
                'Authorization': API_TOKEN,
                'content-type': 'application/json'
            }

        };

        // Request
        post_req = http.request( post_options, function( res )
        {
            res.setEncoding( 'utf8' );

            var body = '';
            // Divided into chunk
            res.on( 'data', function( chunk )
            {
                body += chunk;
            } );

            res.on( 'end', function()
            {
                // Parse json
                var obj = JSON.parse( body );

                var xeroJson = [];

                if( obj.meta.success === true && obj.data.length > 0 )
                {

                    for( var orderCounter in obj.data )
                    {
                        var ordersData = obj.data;
                        if( ordersData.hasOwnProperty( orderCounter ) )
                        {
                            order = ordersData[orderCounter];
                            var items = order.items.data;
                            var xeroItems = [];

                            for( var i = 0; i < items.length; i ++ )
                            {
                                var lastName = order.customer['data']['address']['data']['last_name'];
                                if( lastName )
                                {
                                    lastName = lastName + ', '
                                }

                                xeroItems.push( {
                                    Description: items[i]['description'] == null ? 'Description is empty' : items[i]['description'],
                                    Quantity: items[i]['quantity'],
                                    UnitAmount: items[i]['price'],
                                    ItemCode: items[i]['sku'],
                                    TaxType: 'OUTPUT',
                                    Discount: items[i]['discount'],
                                    AccountCode: 210,
                                } );

                                if( items[i].hasOwnProperty( 'extras' ) )
                                {

                                    var extras = items[i].extras;

                                    for( var j in extras )
                                    {
                                        xeroItems.push( {
                                            Description: extras[j]['name'] == null ? 'Description is empty' : extras[j]['name'],
                                            Quantity: items[i]['quantity'],
                                            UnitAmount: extras[j]['base_price'],
                                            TaxType: 'OUTPUT',
                                            ItemCode: extras[j]['sku'],
                                            Discount: items[i]['discount'],
                                            AccountCode: 210,
                                        } );
                                    }
                                }

                            }
                            xeroJson.push( {
                                Type: 'ACCREC',
                                Contact: {
                                    Name: lastName + order.customer['data']['address']['data']['first_name'],
                                    EmailAddress: order.customer['data']['email'],
                                    Addresses: [
                                        {
                                            AddressLine1: order.customer['data']['address']['data']['street_address'],
                                            City: order.customer['data']['address']['data']['city'],
                                            Region: order.customer['data']['address']['data']['state'],
                                            PostalCode: order.customer['data']['address']['data']['postal_code'],
                                        }
                                    ]
                                },
                                Date: order.invoices['data']['0']['created_at'],
                                DueDate: order.invoices['data']['0']['created_at'],
                                InvoiceNumber: 'INV-GD-' + order.invoices['data']['0']['id'],
                                LineAmountTypes: 'Inclusive',
                                SubTotal: order.invoices['data']['0']['subtotal'],
                                Amount: order.invoices['data']['0']['total'],
                                TotalTax: order.invoices['data']['0']['total'],
                                AmountPaid: order.invoices['data']['0']['payments']['data']['0']['amount'],
                                Status: 'DRAFT',
                                LineItems: xeroItems,
                                BrandingTheme: 'Gameday Invoice',
                            } );
                        }
                    }

                    if( xeroJson.length > 0 )
                    {
                        sendToXero( xeroJson, function( result )
                        {
                            context.done( null, result );
                            console.log( 'Xero has completed its task' );
                        } );
                    }
                }
                else
                {

                    // Set default message
                    var message = '';

                    if( obj.meta.success !== true )
                    {
                        // Set message if status is not true.
                        message = 'This request status returns false';
                    }
                    else if( obj.data.length < 1 )
                    {
                        // Set message if API request did not get any orders
                        message = 'This request does not return any order in between ' + START_DATE + ' to ' + END_DATE;
                    }

                    // Print message
                    console.log( message );
                    context.fail( message );
                }
            } );
        } );

        // End Request
        post_req.end();
    }

    /**
     * Xero
     *    Make Xero object to send request to Xero API
     * @param key
     * @param secret
     * @param rsa_key
     * @param showXmlAttributes
     * @param customHeaders
     * @constructor
     */
    function Xero( key, secret, rsa_key, showXmlAttributes, customHeaders )
    {
        this.key = key;
        this.secret = secret;

        this.parser = new xml2js.Parser( {
            explicitArray: false,
            ignoreAttrs: showXmlAttributes !== undefined ? (showXmlAttributes ? false : true) : true,
            async: true
        } );

        this.oa = new oauth.OAuth( null, null, key, secret, '1.0', null, "PLAINTEXT", null, customHeaders );
        this.oa._signatureMethod = "RSA-SHA1"
        this.oa._createSignature = function( signatureBase, tokenSecret )
        {
            return crypto.createSign( "RSA-SHA1" ).update( signatureBase ).sign( rsa_key, output_format = "base64" );
        }
    }

    function sendToXero( xeroJson, callback )
    {

        Xero.prototype.call = function( method, path, body, callback )
        {
            var self = this;

            var post_body = null;
            var content_type = null;
            if( method && method !== 'GET' && body )
            {
                if( Buffer.isBuffer( body ) )
                {
                    post_body = body;
                }
                else
                {
                    var root = path.match( /([^\/\?]+)/ )[1];
                    post_body = new EasyXml( {rootElement: inflect.singularize( root ), rootArray: root, manifest: true} ).render( body );
                    //console.log(post_body);
                    content_type = 'application/xml';
                }
            }
            var process = function( err, xml, res )
            {
                if( err )
                {
                    return callback( err );
                }

                self.parser.parseString( xml, function( err, json )
                {
                    if( err )
                    {
                        return callback( err );
                    }
                    if( json && json.Response && json.Response.Status !== 'OK' )
                    {
                        return callback( json, res );
                    }
                    else
                    {
                        return callback( null, json, res );
                    }
                } );
            };
            return self.oa._performSecureRequest( self.key, self.secret, method, XERO_API_URL + path, null, post_body, content_type, callback ? process : null );
        }

        module.exports = Xero;

        var xero = new Xero( key, secret, rsa_key );

        xero.call( 'POST', '/Invoices', xeroJson, function( err, json )
        {
            if( err )
            {
                console.log( err );
                context.fail( err );
                //return res.json( 400, {error: 'Unable to contact Xero'} );
            }
            console.log( json );

            callback( json );
        } );
    }

    /**
     * Zero append before digit
     *       If digit in less then 10 we need preappend zero.
     * @param digit
     * @returns {string}
     */
    function zeroAppendBeforeDigit( digit )
    {
        return digit > 9 ? digit : '0' + digit;
    }
};